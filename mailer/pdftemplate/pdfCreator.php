<?php
require("fpdf.php");

class PDF extends FPDF
{
    private $inquiry;
    
    public function __construct($inquiryItem) {
        $this->inquiry = $inquiryItem;
        parent::__construct('P', 'mm', 'A4');
    }
    
    function Header()
    {
        // Logo
//         $this->Image('../../../../resources/img/car_view_3.jpg',30,100,150);
//         $this->Image('../../../../resources/uploads/inquiry/26_car_1541183942.png',-77,100,255);
//        $this->Image('../../../../resources/img/car_view_3.jpg',10,6,30);
        // Arial bold 15
        $this->SetFont('Courier','I',22);
        $this->Cell(75,30, $this->Image(__DIR__.'/../../../../resources/img/logo_3.png',15,12,65),'LTB', 0);
        
        $this->Cell(115,10,'','LTR',2,'C');
        $this->Cell(115,10,$this->inquiry[0]['companyName'],'LR',2,'C');
        
        $this->SetFont('Courier','I',16);
        $this->Cell(115,10,$this->inquiry[0]['companyNumber'],'LRB',0,'R');
        
//        $this->SetFont('Arial','',15);
        // Move to the right
        //this->Cell(80);
        // Title
        //$this->Cell(190,30,$this->inquiry['customerName'],1,0,'C');
        // Line break
        $this->Ln(15);
    }
    
    // Page footer
//     function Footer()
//     {
//         // Position at 1.5 cm from bottom
//         $this->SetY(-2);
//         // Arial italic 8
//         $this->SetFont('Arial','I',8);
//         // Page number
//         $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
//     }
}

function createPdf($inquiry) {
    //$pdf = new PDF('P','mm','A4');
    $pdf = new PDF($inquiry);
    //$pdf->inquiry = array('customerName' => 'Ronak Thakker');
    $pdf->AddPage();
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(95,10,'Customer Name: Ronak Thakker','LT',0, 'B');
    $pdf->Cell(95,10,'Date: 03/11/2018','TR',1, 'B');
    $pdf->Cell(95,10,'Customer No.: 09029765864','L',0, 'B');
    $pdf->Cell(95,10,'Technician Name: Viral Aiya','R',1, 'B');
    $pdf->Cell(95,10,'Car Brand: Hyundai','L',0, 'B');
    $pdf->Cell(95,10,'Pickup Location: Mulund','R',1, 'B');
    $pdf->Cell(95,10,'Car Model: I10','L',0, 'B');
    $pdf->Cell(95,10,'Reaching Time: 15:35','R',1, 'B');
    $pdf->Cell(95,10,'Car Registration No: MH-04-JP-1592','L',0, 'B');
    $pdf->Cell(95,10,'Drop Location: Workshop Name, Kandivali','R',1, 'B');
    $pdf->Cell(95,10,'Odometer Reading: 21551 kms','LB',0, 'B');
    $pdf->Cell(95,10,'Delivery to Workshop: 03/11/2018 20:30!','RB',1, 'B');
    // $pdf->Cell(95,10,'R7C1!',1,0, 'B');
    // $pdf->Cell(95,10,'R7C2!',1,1, 'B');
    
    $pdf->Ln(5);
//     $pdf->Cell(190,110, $pdf->Image(__DIR__.'/../../../../resources/img/car_view_3.jpg',30,115,150),1);
//     $pdf->Image(__DIR__.'../../../../resources/uploads/inquiry/26_car_1541183942.png',-77,115,255);
    //$pdf->Cell(190,130, $pdf->Image('../../../../resources/uploads/inquiry/26_car_1541183942.png',-77,100,255), 1, 1);
    //$pdf->Ln(10);
    $pdf->Ln(110);
    $pdf->Cell(190,10,'Other Comments: Car is awesome',1,1);
    $pdf->Ln(5);
    
    $pdf->Cell(95,10,'Customer Sign By: Ronak Thakker',1,0, 'B');
    $pdf->Cell(95,10,'Workshop Sign By: Dhaval Aiya',1,1, 'B');
    $pdf->Cell(95,30,$pdf->Image('../../../../resources/uploads/inquiry/25_custSign_1540633165.png',30,250,30),1,0);
    $pdf->Cell(95,30,$pdf->Image('../../../../resources/uploads/inquiry/25_wsSign_1540633172.png',135,250,30),1,1);
    return $pdf->Output("", "S");
}
?>
