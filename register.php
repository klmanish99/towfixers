<?php
	include 'config.php';
	require'mailer/PHPMailer/PHPMailerAutoload.php';
    require'mailer/PHPMailer/class.phpmailer.php';
    require'mailer/PHPMailer/class.smtp.php';

	$data = file_get_contents('php://input');
	$data = json_decode( $data, TRUE );
	//echo "<pre>"; print_r($data);exit;
	$carRegNo = $data['carRegNo1'].'-'.$data['carRegNo2'].'-'.$data['carRegNo3'].'-'.$data['carRegNo4'];
	$memberQuery = mysqli_query($con, "SELECT * FROM `member` WHERE `car_reg_no` = '".$carRegNo."' ");
	if ($memberQuery->num_rows != 0) {
		$membership_status = 1;
	} else {
		$membership_status = 0;
	}

	if ($data['brkSpotAdd']) {
		$trimmed = trim($data['brkSpotAdd']);
		$string = str_replace(' ', '+' , $trimmed);
		$address = $string.'+Maharashtra+India';
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key='.GEOCODING_API;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$response = json_decode(curl_exec($ch), true);
		foreach ($response['results'] as $key => $value) {
			$lat = $value['geometry']['location']['lat'];
			$lng = $value['geometry']['location']['lng'];
		}
	} else {
		$lat = '';
		$lng = '';
	}
	$query = mysqli_query($con, "INSERT INTO `customer` SET 
		`telephone` 			= '".$data['phoneNumber']."',
		`firstname` 			= '".$data['username']."',
		`email` 			    = '".$data['email']."',
		`car_reg_no` 			= '".$carRegNo."',
		`car_brand` 			= '".$data['carBrand']."',
		`car_model` 			= '".$data['carModel']."',
		`brk_spot_add` 			= '".$data['brkSpotAdd']."',
		`drop_add` 				= '".$data['dropAdd']."',
		`tow_to`				= '".$data['towTo']."',
		`car_key_avlbl`  		= '".$data['carKeyAvlbl']."',
		`car_parking_condition` = '".$data['carParkCondition']."',
		`brkdown_type` 	  		= '".$data['brkDownType']."',
		`tyres_condition` 		= '".$data['tyresCondition']."',
		`service_type` 		    = '".$data['serviceType']."',
		`service_required` 		= '".$data['serviceReq']."',
		`custom_date` 		    = '".$data['custom_date']."',
		`custom_hour` 		    = '".$data['custom_hours']."',
		`custom_minute` 		= '".$data['custom_minutes']."',
		`meridiem` 		        = '".$data['meridiem']."',
		`additional_info` 		= '".$data['additionalInfo']."',
		`membership_status`		= '".$membership_status."',
		`lat` 					= '".$lat."',
		`lng` 					= '".$lng."',
		`status` 				= 1,
		`customer_group_id`		= 1,
		`date_added`			= NOW()
	");
	if ($query) {
		if ($data['serviceReq'] == 'Immediately') {
			$serviceReq = $data['serviceReq'];
		}
		else{
			$serviceReq ='Date: '.$data['custom_date'].' Time: '.$data['custom_hours'].':'.$data['custom_minutes'].''.$data['meridiem'];
		}
		$last_id = $con->insert_id;

		function distance($latitude_out, $longitude_out, $latitude, $longitude, $unit) {
		  	$theta = $longitude_out - $longitude;
		  	$dist = sin(deg2rad($latitude_out)) * sin(deg2rad($latitude)) +  cos(deg2rad($latitude_out)) * cos(deg2rad($latitude)) * cos(deg2rad($theta));
		  	$dist = acos($dist);
		  	$dist = rad2deg($dist);
		  	$miles = $dist * 60 * 1.1515;
		  	$unit = strtoupper($unit);
		  	if ($unit == "K") {
		    	return ($miles * 1.609344);
		  	} else if ($unit == "N") {
		      	return ($miles * 0.8684);
		    } else {
		      	return $miles;
		    }
		}

		$vendorIds = mysqli_query($con2, "SELECT `idn_company`, `txt_mobile1`, `lat`, `lng` FROM company WHERE ind_active = 1 AND lat != '' AND lng !='' ");
		foreach ($vendorIds as $vendor_info) {
			$db_latitude  = $vendor_info['lat'];
			$db_longitude = $vendor_info['lng'];

			$dist = distance($lat, $lng, $db_latitude, $db_longitude, "K");
			if($dist <= '3') {
				$insertQuery = mysqli_query($con2,"INSERT INTO `towfixers_inquiry` SET 
					`idn_company` 			= '".$vendor_info['idn_company']."',
					`customer_id`			= '".$last_id."',
					`car_brand` 			= '".$data['carBrand']."',
					`car_model` 			= '".$data['carModel']."',
					`pickup_location` 		= '".$data['brkSpotAdd']."',
					`drop_add` 				= '".$data['dropAdd']."',
					`car_parking_condition` = '".$data['carParkCondition']."',
					`brkdown_type` 	  		= '".$data['brkDownType']."',
					`tyres_condition` 		= '".$data['tyresCondition']."',
					`service_required` 		= '".$serviceReq."',
					`additional_info` 		= '".$data['additionalInfo']."',
					`dt_added`				= '".date("Y-m-d H:i:s")."'
				");
				if ($insertQuery) {
					$msgToVendor = "A new enquiry has been assigned to you.%0aFor More Click http://carfixers.in";
			        $msgToVendor = str_replace(' ', '%20', $msgToVendor);
			        $link = MESSAGE.$vendor_info['txt_mobile1']."&msg=".$msgToVendor.""; 
			        //echo $link;
			        file($link);
				}
			}
		}
		if ($data['towTo'] == 'Local Garage') {
			//$phoneNum = '9833174112';
	        $phoneNum = '9022427049';
			$msgToTech = "Customer name ".$data['username']." has chosen LOCAL GARAGE for his vehicle.%0aCustomer Details:%0aCustomer Name : ".$data['username']."%0aPhone No : ".$data['phoneNumber']."%0aReg No : ".$data['carRegNo']."%0aVehicle make : ".$data['carBrand']."%0aVehicle model : ".$data['carModel']." ";
	        $msgToTech = str_replace(' ', '%20', $msgToTech);
	        $link = MESSAGE.$phoneNum."&msg=".$msgToTech.""; 
	        //echo $link;exit;
	        file($link);
		}
		$phoneNum = $data['phoneNumber'];
		$msgToTech = "Dear ".$data['username']." ".','."Thank you for registration.%0aYou will receive price quote for roadside assistance service in couple of minutes.%0aIf you don't get any response in 5 minutes you can call on 9833174112.";
        $msgToTech = str_replace(' ', '%20', $msgToTech);
        $link = MESSAGE.$phoneNum."&msg=".$msgToTech.""; 
        //echo $link;exit;
        file($link);

        //$phoneNum = '9833174112';
        $phoneNum = '9022427049';
		$msgToTech = "You have one inquiry from ".$data['username']." .";
        $msgToTech = str_replace(' ', '%20', $msgToTech);
        $link = MESSAGE.$phoneNum."&msg=".$msgToTech.""; 
        //echo $link;exit;
        file($link);
        //You will receive price quote for roadside assistance service in couple of minutes.
        //If you don't get any response in 5 minutes you can call on 9833174112.'

        $body = "A new case has been assigned to you from ".$data['username']." . For More Click http://towfixers.in/admin/";
	    $username = 'automail.carfixers@gmail.com';
	    $password = 'carFixers123';

	    //$mail->SMTPDebug = 3;
	    $mail = new PHPMailer();
	    $mail->IsSMTP();
	    $mail->SMTPAuth = true;
	    $mail->Helo = "HELO";
	    $mail->Host = 'smtp.gmail.com';
	    $mail->Port = '587';
	    $mail->Username = $username;
	    $mail->Password = $password;
	    $mail->SMTPSecure = 'tls';
	    $mail->isHTML(true);
	    $mail->SetFrom('automail.carfixers@gmail.com', 'towfixers');
	    $mail->Subject = 'New Enquiry';
	    $mail->Body = html_entity_decode($body);
	    //$mail->AddAddress('towfixersindia@gmail.com');
	   	$mail->AddAddress('shaikhabdullah1995@gmail.com');
	    if(!$mail->Send()) {
	        //echo "<pre>"; print_r($mail->ErrorInfo);
	       	$data = json_encode(["success" => 0 ]);
	    }
	    else {
	        $data = json_encode(["success" => 1 ]);
	    }

		$data = json_encode(["success" => 1 ]);
	} else {
		$data = json_encode(["success" => 0 ]);
	}
	echo $data;
?>