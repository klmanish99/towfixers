<?php date_default_timezone_set('Asia/Kolkata'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="theme-color" content="#eeeeee"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="Get easy access to roadside assistance services in your city.">
    <meta name="author" content="towFixers">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
	<meta property="og:site_name" content="" /> <!-- website name -->
	<meta property="og:site" content="" /> <!-- website link -->
	<meta property="og:title" content=""/> <!-- title shown in the actual shared post -->
	<meta property="og:description" content="" /> <!-- description shown in the actual shared post -->
	<meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
	<meta property="og:url" content="" /> <!-- where do you want your post to link to -->
	<meta property="og:type" content="article" />

    <!-- Website Title -->
    <title>towFixers</title>
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link href="css/swiper.css" rel="stylesheet">
	<link href="css/magnific-popup.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!-- Favicon  -->
    <link rel="icon" href="images/favicon.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body data-spy="scroll" data-target=".fixed-top">
    
    <!-- Preloader -->
	<div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- end of preloader -->
    

    <!-- Navbar -->
    <nav class="navbar navbar-expand-md navbar-dark navbar-custom fixed-top">
        <!-- Text Logo - Use this if you don't have a graphic logo -->
        <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Aria</a> -->

        <!-- Image Logo -->
        <a class="navbar-brand logo-image" href="index.php"><img src="images/final_logo.jpg" style="height: 1.2rem"></a>
        
        <!-- Mobile Menu Toggle Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-awesome fas fa-bars"></span>
            <span class="navbar-toggler-awesome fas fa-times"></span>
        </button>
        <!-- end of mobile menu toggle button -->

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#header">HOME <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#intro">REQUEST SERVICE</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link page-scroll" href="#projects">OUR WORKS</a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#about">ABOUT US</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#services">SERVICES</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#projects">GALLERY</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#resources">RESOURCES</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#contactus">CONTACT</a>
                </li>
                <!-- Dropdown Menu -->          
                <!-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle page-scroll" href="#about" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">ABOUT</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="terms-conditions.html"><span class="item-text">TERMS CONDITIONS</span></a>
                        <div class="dropdown-items-divide-hr"></div>
                        <a class="dropdown-item" href="privacy-policy.html"><span class="item-text">PRIVACY POLICY</span></a>
                    </div>
                </li> -->
                <!-- end of dropdown menu -->

                <!-- <li class="nav-item">
                    <a class="nav-link page-scroll" href="#contact">CONTACT</a>
                </li> -->
            </ul>
            <span class="nav-item social-icons">
                <span class="fa-stack">
                    <a href="https://www.facebook.com/Towfixers-100396308203697/?modal=admin_todo_tour ">
                        <i class="fab fa-facebook-f fa-stack-1x"></i>
                    </a>
                </span>
                <span class="fa-stack">
                    <a href="https://twitter.com/carfixers ">
                        <i class="fab fa-twitter fa-stack-1x"></i>
                    </a>
                </span>
            </span>
        </div>
    </nav> <!-- end of navbar -->
    <!-- end of navbar -->


    <!-- Header -->
    <header id="header" class="header">
        <div class="header-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-container">
                            <!-- <h1>LOREM <span id="js-rotating">IPSUM, DOLOR, AMET</span></h1> -->
                            <h1>Roadside Assistance Service 24 x 7</h1>
                           <!--  <p class="p-heading p-large">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p> -->
                            <a class="btn-solid-lg page-scroll" href="#intro">Request Service</a>
                        </div>
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
    </header> <!-- end of header -->
    <!-- end of header -->


    <!-- Intro -->
    <div id="intro" class="basic-1">
        <div class="myContainer" id="logIn_container">
            <span class="formTitle">Enter your phone number to proceed</span>
            <div class="inputPhoneNum">
                <label style="margin-bottom:0px" class="pure-material-textfield-standard">
                    <input id="phonNumber" type="number" name="phonNumber" placeholder=" " autocomplete="off">
                    <span class="phonNumber_span">Phone Number</span>
                </label> 
                <p class="error_phonNumber">Please enter 10 digit phone number</p>  
            </div>
            <button class="btnSubmit" id="btnLogin">
                <span id="txtNxt">Next</span>
                <div style="display: none;" id="txtNxtLoading">
                    <i class="fa fa-circle-o-notch fa-spin"></i>Loading
              </div>
            </button>
        </div>

        <div class="myContainer" style="display: none;" id="otp_container">
            <span class="formTitle">Enter OTP </span>
            <div class="inputPhoneNum">
                <label style="margin-bottom:0px" class="pure-material-textfield-standard">
                    <input id="otp" type="number" name="otp" placeholder=" " autocomplete="off">
                    <span class="otp_span">OTP</span>
                </label> 
                <p class="error_phonNumber error_otp">Please enter OTP</p>
                <p class="error_phonNumber error_wrong_otp">Invalid OTP</p>
            </div>
            <button class="btnSubmit" id="btnOtp">
                <span id="txtSubmit">Submit</span>
                <div style="display: none;" id="txtSubmitLoading">
                    <i class="fa fa-circle-o-notch fa-spin"></i>Loading
              </div>
            </button>
        </div>

        <div class="regContainer" style="display: none;" id="regContainer">
            <span class="formTitle">Register</span>
            <label class="pure-material-textfield-standard">
                <input id="username" type="text" name="username" placeholder=" " autocomplete="off">
                <span class="username">Customer name</span>
            </label>

             <label class="pure-material-textfield-standard">
                <input id="email" type="text" name="email" placeholder=" " autocomplete="off">
                <span class="email">Email</span>
            </label>

            <span class="carRegNo">Car Reg No</span>
            <div class="carRegNo flex">
                <input style="width: 30px;" id="carRegNo1" type="text" onkeyup="movetoNext(this, 'carRegNo2', 'carRegNo1')" maxlength="2" name="carRegNo1" oninput="this.value = this.value.toUpperCase()" onkeypress="return /[a-z]/i.test(event.key)" autocomplete="off">-
                <input style="width: 30px;" id="carRegNo2" type="number" onkeyup="movetoNext(this, 'carRegNo3','carRegNo1')" maxlength="2" name="carRegNo2" oninput="this.value = this.value.toUpperCase()" autocomplete="off">-
                <input style="width: 30px;" id="carRegNo3" type="text" onkeyup="movetoNext(this, 'carRegNo4','carRegNo2')" maxlength="2" name="carRegNo3" oninput="this.value = this.value.toUpperCase()" onkeypress="return /[a-z]/i.test(event.key)" autocomplete="off">-
                <input style="width: 45px;" id="carRegNo4" type="number" name="carRegNo4" onkeyup="movetoNext(this, 'carBrand', 'carRegNo3')" maxlength="4" oninput="this.value = this.value.toUpperCase()" autocomplete="off">
            </div>
            <label class="pure-material-textfield-standard">
                <input id="carBrand" type="text" name="carBrand" placeholder=" " onkeyup="movetoNext(this, 'carBrand', 'carRegNo4')" autocomplete="off">
                <span class="carBrand">Car Brand</span>
            </label>
            <label class="pure-material-textfield-standard">
                <input id="carModel" type="text" name="carModel" placeholder=" " autocomplete="off">
                <span class="carModel">Car Model</span>
            </label>

            <label class="pure-material-textfield-standard">
                <input id="brkSpotAdd" type="text" name="brkSpotAdd" placeholder=" " autocomplete="off">
                <span class="brkSpotAdd">Breakdown Spot Address</span>
                <span id="brkSpotAdd_info" style="display: none; font-size: 12px; color: green;">Only area name, Example : Andheri E</span>
            </label>

            <label class="pure-material-textfield-standard">
                <input id="dropAdd" type="text" name="dropAdd" placeholder=" " autocomplete="off">
                <span class="dropAdd">Drop Address</span>
            </label>

            <div class="inputGroup">
                <input id="radio1" name="towTo" value="Dealership" type="radio"/>
                <label for="radio1">Company Authorized service centre</label>
            </div>
            <div class="inputGroup">
                <input id="radio2" name="towTo" value="Local Garage" type="radio"/>
                <label for="radio2">Local Garage/Known Mechanic</label>
            </div>

            <div style="display: flex; margin-top: 20px; align-items: center;justify-content: space-between;">
                <div style="display: flex;">
                    <label class="pure-material-textfield-standard">Car Keys Available :</label>
                    <label style="display: none; padding-left: 10px;" id="yes">Yes</label>
                    <label style="padding-left: 10px;" id="no">No</label>
                </div>
                <label class="switch">
                  <input id="carKeyAvlbl" type="checkbox" name="carKeyAvlbl">
                  <span class="myslider round"></span>
                </label>
            </div>

            <select id="carParkCondition" class="selectOption" name="carParkCondition">
                <option value="" disabled selected>Car Parking condition</option>
                <option value="Underground Basement">Underground Basement</option>
                <option value="Upper level parking">Upper level parking</option>
                <option value="Road level parking">Road level parking</option>
                <option value="On Road">On Road</option>
                
            </select>

            <select id="brkDownType" class="selectOption" name="brkDownType">
                <option value="" disabled selected>Breakdown Type</option>
                <option value="Mechanical breakdown">Mechanical breakdown</option>
                <option value="Accidental breakdown">Accidental breakdown</option>
            </select>

            <label id="tyreLabel" class="pure-material-textfield-standard">
                <input id="tyresCondition" type="text" name="tyresCondition" placeholder=" " autocomplete="off">
                <span class="tyresCondition">Tyres Condtion</span>
            </label>

            <select id="serviceType" class="selectOption" name="serviceType">
                <option value="" disabled selected>Type of Service</option>
                <option value="Car towing">Car towing (Flatbed/Zero Degree/Underlift)</option>
                <option value="Bike Towing">Bike Towing</option>
                <option value="Battery Jump Start">Battery Jump Start</option>
            </select>

            <select id="serviceReq" class="selectOption" name="serviceReq">
                <option value="" disabled selected>Service Required</option>
                <option value="Immediately">Immediately</option>
                <option value="Custom Date Time">Custom Date & Time</option>
            </select>

            <label class="pure-material-textfield-standard">
                <input id="custom_date" type="text" name="custom_date" placeholder=" " value="<?php echo date('d-m-Y'); ?>" autocomplete="off">
                <span class="custom_date">Date</span>
            </label>

            <div id="custom_time" class="col-sm-12 custom_time" style="padding: 0px">     
                <select id="custom_hours" class="selectOption custom_hours" name="custom_hours" style="width: 30%">
                    <option value="" disabled selected>Hours</option>
                    <?php for ($i = 1; $i <= 12; $i++){ 
                        $hr = str_pad($i,2,"0",STR_PAD_LEFT);
                    ?>
                        <option value='<?php echo $hr; ?>'><?php echo $hr; ?></option>
                    <?php } ?>
                </select> 
                <select id="custom_minutes" class="selectOption custom_minutes" name="custom_minutes" style="width: 30%">
                   	<option value="" disabled selected>Minutes</option>
                    <?php for ($i=0;$i<=55;$i+=5){ 
                        $mnts = str_pad($i,2,"0",STR_PAD_LEFT);
                    ?>
                        <option value='<?php echo $mnts; ?>'><?php echo $mnts; ?></option>
                    <?php } ?>
                </select>
                 <select id="meridiem" class="selectOption meridiem" name="meridiem" style="width: 10%">
                    <option value="AM">AM</option>
                    <option value="PM" selected="selected">PM</option>
                </select>
            </div>
            <label class="pure-material-textfield-standard">
                <input id="additionalInfo" type="text" name="additionalInfo" placeholder=" " autocomplete="off">
                <span class="additionalInfo">Any Additional Information</span>
            </label>

           
            <input class="btnSubmit" id="btnReg" type="button" value="Register">

        </div>
    </div> <!-- end of basic-1 -->

    <div style="display: none;" class="popUpSuccessContainer">
        <div class="popUpSuccess">
            <div class="popUpSuccessTop">
                <img src="images/success.png">
            </div>
            <div class="popUpSuccessBottom">
                <span style="text-align: center; font-size: 20px; margin: 10px; color: green">Successfully Registered.</span>
                <span style="text-align: center;font-size: 10px;line-height: 15px;color: black; margin: 5px;">Please wait for 5 minutes to get quotes from the nearest reliable towing vendors.</span>
                
                <input style="background-color:#3d91f7" class="btnSubmit" id="btnOk" type="button" value="Ok">
            </div>
        </div>
    </div>



<script type="text/javascript">
    //VALIDATE PHONE NUMBER
    $('#phonNumber').on('keyup', function(){
        var phoneNum = $(this).val();
        if (phoneNum.length != 10) {
            $('.error_phonNumber').css("display","block");
            $('.phonNumber_span').css("border-bottom","1px solid red");  
        }
        else{
            $('.error_phonNumber').css("display","none");
            $('.phonNumber_span').css("border-bottom","1px solid black");
        }
    });
    //END OF VALIDATE

    //Car Reg Number auto move to next field starts here.
    function movetoNext(current, nextFieldID, prevFieldID) {
        if (current.value.length >= current.maxLength) {
            document.getElementById(nextFieldID).focus();
        }
        if (current.value.length === 0) {
            document.getElementById(prevFieldID).focus();    
        }
    }
    //Car Reg Number auto move to next field ends here.

    //autocomplete
    $( "#carBrand" ).autocomplete({
      source: function( request, response ) {
       // Fetch data
       $.ajax({
        url: "fetchData.php",
        type: 'post',
        dataType: "json",
        data: {
         search: request.term
        },
        success: function( data ) {
         response( data );
        }
       });
      },
      select: function (event, ui) {
       // Set selection
       $('#carBrand').val(ui.item.label); // display the selected text
       $('#selectuser_id').val(ui.item.value); // save selected id to input
       return false;
      }
     });

    //Login start
    $('#btnLogin').on('click', function(){
        var phonNumber = $('#phonNumber').val();
        if (phonNumber.length != 10) {
            $('.error_phonNumber').css("display","block");
            $('.phonNumber_span').css("border-bottom","1px solid red");
            event.preventDefault()
        }
        else{
            $(this).attr("disabled","disabled");
            $('#txtNxt').css("display","none");
            $('#txtNxtLoading').removeAttr("style");
            var data = JSON.stringify({"type" : "LOGIN", "phonNumber" : phonNumber});
            request = $.ajax({
                contentType: "application/json; charset=utf-8",
                method: "POST",
                url: "backEnd.php",
                data: data,
                dataType : "json",
            });
            request.done(function (response, textStatus, jqXHR){
                if (response.success === 1) {
                    $('#logIn_container').hide(100);
                    $('#otp_container').show();
                }
                console.log(response);
            });
            $('.error_phonNumber').css("display","none");
            $('.phonNumber_span').css("border-bottom","1px solid black");
            event.preventDefault()
        }
    });
    //login end

    //otp start 
    $('#btnOtp').on('click', function(){
        var otp = $('#otp').val();
        var phonNumber = $('#phonNumber').val();
        if (otp.length == 0) {
            $('.error_otp').css("display","block");
            $('.otp_span').css("border-bottom","1px solid red");
            event.preventDefault()
        } else {
            $(this).attr("disabled","disabled");
            $('#txtSubmit').css("display","none");
            $('#txtSubmitLoading').removeAttr("style");
            var data = JSON.stringify({"type" : "OTP", "otp" : otp, "phoneNumber" : phonNumber});
            request = $.ajax({
                contentType: "application/json; charset=utf-8",
                method: "POST",
                url: "backEnd.php",
                data: data,
                dataType : "json",
            });
            request.done(function (response, textStatus, jqXHR){
                if (response.success === 1) {
                    $('#otp_container').hide(100);
                    $('#regContainer').show();
                    console.log(response);
                }
                else{
                    $('.error_wrong_otp').css("display","block");
                    setTimeout(function(){ window.location = 'index.php'; }, 2000);
                }
            });
            $('.error_otp').css("display","none");
            $('.otp_span').css("border-bottom","1px solid black");
            event.preventDefault()
        }
    });
    //otp end

    $('#carKeyAvlbl').on('change', function(){
        if (this.checked) {
            $('#yes').show();
            $('#no').hide();
        }
        else{
            $('#no').show();
            $('#yes').hide();
        }
    });
    
    //INFO BREAKDOWN SPOT START
    //$('#brkSpotAdd').on('click', function(){
        //$('#brkSpotAdd_info').css("display","block");
    //});

    
    //INFO BREAKDOWN SPOT END

    // $('#brkDownType').on('change', function(){
    //         var a = $(this).val();
    //         alert(a);
    //     });
    
    //datepicker
     $( "#custom_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
    //Register Start
    $('#btnReg').on('click', function(){
        $('#loader').css("display","flex");
        $('html').css("overflow","hidden");
        var phonNumber = $('#phonNumber').val();
        var username = $('#username').val();
        var email    = $('#email').val();
        var carRegNo1 = $('#carRegNo1').val();
        var carRegNo2 = $('#carRegNo2').val();
        var carRegNo3 = $('#carRegNo3').val();
        var carRegNo4 = $('#carRegNo4').val();
        var carBrand = $('#carBrand').val();
        var carModel = $('#carModel').val();
        var brkSpotAdd = $('#brkSpotAdd').val();
        var dropAdd = $('#dropAdd').val();
        var carParkCondition = $('#carParkCondition').val();
        var brkDownType = $('#brkDownType').val();
        var tyresCondition = $('#tyresCondition').val();
        var serviceType = $('#serviceType').val();
        var serviceReq = $('#serviceReq').val();
        var towTo = $('input[name=towTo]:checked').val();
        if(serviceReq === 'Custom Date Time'){
        	var custom_date = $('#custom_date').val();
        	var custom_hours = $('#custom_hours').val();
        	var custom_minutes = $('#custom_minutes').val();
        	var meridiem = $('#meridiem').val();
        }else{
        	var custom_date = '';
        	var custom_hours = '';
        	var custom_minutes = '';
        	var meridiem = '';
        }
        
        //return false;
        var additionalInfo = $('#additionalInfo').val();
        var carKeyCheck = $('#carKeyAvlbl:checked').val();
        if (carKeyCheck == "on") {
            carKeyAvlbl = "Yes";
        } else {
            carKeyAvlbl = "No";
        }
        
        var data = JSON.stringify({
            "phoneNumber":phonNumber,
            "username": username,
            "email": email,
            "carRegNo1": carRegNo1,
            "carRegNo2": carRegNo2,
            "carRegNo3": carRegNo3,
            "carRegNo4": carRegNo4,
            "carBrand": carBrand,
            "carModel": carModel,
            "brkSpotAdd": brkSpotAdd,
            "dropAdd": dropAdd,
            "carKeyAvlbl": carKeyAvlbl,
            "carParkCondition": carParkCondition,
            "brkDownType": brkDownType,
            "serviceType": serviceType,
            "serviceReq" : serviceReq,
            "towTo": towTo,
            "custom_date" : custom_date,
            "custom_hours" : custom_hours,
            "custom_minutes" : custom_minutes,
            "meridiem" : meridiem,
            "additionalInfo":additionalInfo,
            "tyresCondition": tyresCondition
            });
            request = $.ajax({
                contentType: "application/json; charset=utf-8",
                method: "POST",
                url: "register.php",
                data: data,
                dataType : "json",
            });
            request.done(function (response, textStatus, jqXHR){
                if (response.success === 1) {
                    $('.popUpSuccessContainer').css("display","flex");
                    $('html').css("overflow","hidden");
                }
                else{
                    alert("Sorry");
                }
            });
    });
    $('#brkDownType').on('change',function(){
       var brktype = $('#brkDownType').val();
       if(brktype === 'Mechanical breakdown'){
            $('#tyreLabel').hide();
       }else{
            $('#tyreLabel').show();
       }
    });
    $('#serviceReq').on('change',function(){
    	var service = $('#serviceReq').val();
    	if(service === 'Custom Date Time'){
    		$('#custom_date').show();
    		$('#custom_time').show();
    	}else{
    		$('#custom_date').hide();
    		$('#custom_time').hide();
    	}
    })
    //Register End
    $('#btnOk').on('click', function(){
        window.location = 'index.php';
    });
</script>
    <!--About-->
    <div id="about" class="filter bg-light">
        
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">ABOUT US</div>
                    <p  style="text-align: justify;">With over 7 years of experience in roadside assistance industry, the company has made a good reputation amongst car dealers and car owners in providing quick responsive and safe car towing services. 'Towfixers' has been created to have an online presence and to serve our customers in a better way. Towfixers will save your time in finding the best, nearest and fairly priced towing service in your city. Customers will get instant quotes and the time of arrival of their chosen towing vendor within minutes. We aspire to the best in our service offerings and will keep pushing the boundaries.</p>
                </div> <!-- end of col -->
            </div>
        </div>
        
    </div>
    <!--Services -->
    <div id="services" class="filter">
        <div class="container">
            <div class="row">
                 <div class="col-lg-12">
                    <div class="section-title">SERVICES</div>
                </div>
                <div class="col-lg-3">
                    <div class="card  mb-3" >
                        <div class="card-header bg-light card-shadow">Flatbed Towing</div>
                        <div class="card-body card-shadow">
                            <p class="card-text" style="text-align: justify;">Flatbed tow trucks acquire their name from their bed’s flat appearance, making them the favorable choice... </p>
                            <a class="popup-with-move-anim" href="#flatbed">Read more...</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card  mb-3">
                        <div class="card-header bg-light card-shadow">Hydraulic Underlift Towing</div>
                        <div class="card-body card-shadow" style="max-height: 500px">
                            <p class="card-text" style="text-align: justify;">Hydraulic underlift tow trucks lift the vehicle off of the ground by securing either the front... </p>
                            <a class="popup-with-move-anim" href="#hydraulic">Read more...</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card  mb-3">
                        <div class="card-header bg-light card-shadow">Bike Flatbed Towing</div>
                        <div class="card-body card-shadow">
                            <p class="card-text" style="text-align: justify;">The bike is hoisted entirely off of the ground onto the truck bed. This modern technique of towing a bike...</p>
                            <a class="popup-with-move-anim" href="#bikeFlatbed">Read more...</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card mb-3">
                        <div class="card-header bg-light  card-shadow">Battery Jump Start Service</div>
                        <div class="card-body card-shadow">
                            <p class="card-text" style="text-align: justify;">We provide car battery jump start service in your city. Our technician will reach your location with the...</p>
                            <a class="popup-with-move-anim" href="#batteryJump">Read more...</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <div id="flatbed" class="lightbox-basic zoom-anim-dialog mfp-hide">
        <div class="row">
            <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
            <div class="col-lg-12">
                <h6>Flatbed Towing</h6>
                <p  style="text-align: justify;">Flatbed tow trucks acquire their name from their bed’s flat appearance, making them the favorable choice for a safe and secure tow.With an ability to hoist a vehicle entirely off of the ground onto the truck bed, flatbed trucks are ideal for circumstances where utilizing other means of towing are either not possible due to the current state of the vehicle, or the vehicle’s model and fragile nature of its characteristics. These tows are commonly directed toward classic, exotic or all-wheel drive vehicles, as well as motorcycles, vehicles with very low ground clearance, and auto-accident vehicles that are significantly damaged.</p>
            </div> 
        </div>
    </div>
    <div id="hydraulic" class="lightbox-basic zoom-anim-dialog mfp-hide">
        <div class="row">
            <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
            <div class="col-lg-12">
                <h6>Hydraulic Underlift Towing</h6>
                <p  style="text-align: justify;">Hydraulic underlift tow trucks lift the vehicle off of the ground by securing either the front two or back two wheels. The other pair of wheels remains in contact with the road while the vehicle is being towed to assist in the transportation process. Underlift tow trucks are equipped with a modern wheel-system, which eliminates all tow truck and vehicle contact (with an exception of the rubber on a vehicle’s tires). This makes it a safe & a preferred method of towing a car.</p>
            </div> 
        </div>
    </div>
    <div id="bikeFlatbed" class="lightbox-basic zoom-anim-dialog mfp-hide">
        <div class="row">
            <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
            <div class="col-lg-12">
                <h6>Bike Flatbed Towing</h6>
                <p  style="text-align: justify;">The bike is hoisted entirely off of the ground onto the truck bed. This modern technique of towing a bike makes the transportation process much safer and reliable.</p>
            </div> 
        </div>
    </div>
    <div id="batteryJump" class="lightbox-basic zoom-anim-dialog mfp-hide">
        <div class="row">
            <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
            <div class="col-lg-12">
                <h6>Battery Jump Start Service</h6>
                <p  style="text-align: justify;">We provide car battery jump start service in your city. Our technician will reach your location with the equipment and jump start the vehicle in cases of dead/weak battery. </p>
            </div> 
        </div>
    </div>
    <!-- Projects -->
	<div id="projects" class="filter">
		<div class="container">
            
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">OUR WORKS</div>
                    <h2>Our Works That We're Proud Of</h2>
                </div> 
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="grid">
                        <div class="element-item development">
                            <a class="popup-with-move-anim" href="#project-1"><div class="element-item-overlay"><span>JUMPSTART SERVICE</span></div><img  src="images/img-1.jpg"  alt="alternative"></a>
                        </div>
                        <!-- <div class="element-item development">
                            <a class="popup-with-move-anim" href="#project-2"><div class="element-item-overlay"><span>JUMPSTART SERVICE</span></div><img  src="images/img-2.jpg" alt="alternative"></a>
                        </div>
                        <div class="element-item design development marketing">
                            <a class="popup-with-move-anim" href="#project-3"><div class="element-item-overlay"><span>BIKE TOWING</span></div><img  src="images/img-3.jpg" alt="alternative"></a>
                        </div> -->
                        <div class="element-item design development marketing">
                            <a class="popup-with-move-anim" href="#project-4"><div class="element-item-overlay"><span>BIKE TOWING</span></div><img  src="images/img-4.jpg" alt="alternative"></a>
                        </div>
                        <div class="element-item design development marketing seo">
                            <a class="popup-with-move-anim" href="#project-5"><div class="element-item-overlay"><span>UNDERLIFT</span></div><img  src="images/img-5.jpg" alt="alternative"></a>
                        </div>
                       <!--  <div class="element-item design marketing seo">
                            <a class="popup-with-move-anim" href="#project-6"><div class="element-item-overlay"><span>UNDERLIFT</span></div><img  src="images/img-6.jpg" alt="alternative"></a>
                        </div>
                        <div class="element-item design marketing">
                            <a class="popup-with-move-anim" href="#project-7"><div class="element-item-overlay"><span>BIKE TOWING</span></div><img  src="images/img-7.jpg" alt="alternative"></a>
                        </div> -->
                        <div class="element-item design marketing">
                            <a class="popup-with-move-anim" href="#project-8"><div class="element-item-overlay"><span>ZERO DEGREE FLATBED</span></div><img  src="images/img-8.jpg" alt="alternative"></a>
                        </div>
                    </div> <!-- end of grid -->
                    <!-- end of filter -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
		</div> <!-- end of container -->
    </div> <!-- end of filter -->
    <!-- end of projects -->
    <!--Resources -->
    <div id="resources" class="filter">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="section-title">RESOURCES</div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12" style= "display: flex;flex-wrap: wrap;justify-content: center;align-items: center;">
                        <iframe style="padding: 10px" src="https://www.youtube.com/embed/f4nCuaEwiio" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <iframe style="padding: 10px" src="https://www.youtube.com/embed/3MlYlNmeQkY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--Contact us -->
    <div id="contactus" class="filter">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="section-title">Contact Us</div>
                </div>
            </div>
            <div class="basic-1" style="padding-top: 0px !important;">
                <div class="myContainer" style="justify-content: unset !important;">
                    <div class="contactInfo">
                        <label style="margin-bottom: 10px;">
                            <a style="text-decoration: none;" href="mailto:towfixersindia@gmail.com">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i><span>towfixersindia@gmail.com</span>
                            </a>
                        </label>
                        <label>
                            <a style="text-decoration: none;" href="tel:9833174112">
                                <i style="margin-right: 15px;" class="fa fa-phone" aria-hidden="true"></i><span>9833174112</span>
                            </a>
                        </label>
                    </div>
                    <label class="pure-material-textfield-standard">
                        <input id="mobileNumber" type="number" name="mobileNumber" placeholder=" " autocomplete="off">
                        <span class="mobileNumber">Phone Number</span>
                    </label>

                     <label class="pure-material-textfield-standard">
                        <input id="contact_email" type="email" name="contact_email" placeholder=" " autocomplete="off">
                        <span class="contact_email">Email</span>
                    </label>

                    <label class="pure-material-textfield-standard">
                        <textarea id="message"  name="message" placeholder=" " autocomplete="off"></textarea>
                        <span class="message">Message</span>
                    </label>
                    <button class="btnSubmit" id="btnContactus">
                        <span id="txtcontact">Submit</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Project Lightboxes -->
    <!-- Lightbox -->
    <div id="project-1" class="lightbox-basic zoom-anim-dialog mfp-hide">
        <div class="row">
            <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
            <div class="col-lg-12">
                <img class="img-fluid" style="display: flex;" src="images/img-1.jpg" alt="alternative">
            </div> <!-- end of col -->
            <!-- <div class="col-lg-4">
                <h3>Online Banking</h3>
                <hr class="line-heading">
                <h6>Strategy Development</h6>
                <p>Need a solid foundation for your business growth plans? Aria will help you manage sales and meet your current needs</p>
                <p>By offering the best professional services and quality products in the market. Don't hesitate and get in touch with us.</p>
                <div class="testimonial-container">
                    <p class="testimonial-text">Need a solid foundation for your business growth plans? Aria will help you manage sales and meet your current requirements.</p>
                    <p class="testimonial-author">General Manager</p>
                </div>
                <a class="btn-solid-reg" href="#your-link">DETAILS</a> <a class="btn-outline-reg mfp-close as-button" href="#projects">BACK</a> 
            </div> --> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of lightbox-basic -->
    <!-- end of lightbox -->

    <!-- Lightbox -->
    <div id="project-2" class="lightbox-basic zoom-anim-dialog mfp-hide">
        <div class="row">
            <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
            <div class="col-lg-12">
                <img class="img-fluid" src="images/img-2.jpg" alt="alternative">
            </div> <!-- end of col -->
            <!-- <div class="col-lg-4">
                <h3>Classic Industry</h3>
                <hr class="line-heading">
                <h6>Strategy Development</h6>
                <p>Need a solid foundation for your business growth plans? Aria will help you manage sales and meet your current needs</p>
                <p>By offering the best professional services and quality products in the market. Don't hesitate and get in touch with us.</p>
                <div class="testimonial-container">
                    <p class="testimonial-text">Need a solid foundation for your business growth plans? Aria will help you manage sales and meet your current requirements.</p>
                    <p class="testimonial-author">General Manager</p>
                </div>
                <a class="btn-solid-reg" href="#your-link">DETAILS</a> <a class="btn-outline-reg mfp-close as-button" href="#projects">BACK</a> 
            </div> --> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of lightbox-basic -->
    <!-- end of lightbox -->

    <!-- Lightbox -->
    <div id="project-3" class="lightbox-basic zoom-anim-dialog mfp-hide">
        <div class="row">
            <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
            <div class="col-lg-12">
                <img class="img-fluid" src="images/img-3.jpg" alt="alternative">
            </div> <!-- end of col -->
           <!--  <div class="col-lg-4">
                <h3>BoomBap Audio</h3>
                <hr class="line-heading">
                <h6>Strategy Development</h6>
                <p>Need a solid foundation for your business growth plans? Aria will help you manage sales and meet your current needs</p>
                <p>By offering the best professional services and quality products in the market. Don't hesitate and get in touch with us.</p>
                <div class="testimonial-container">
                    <p class="testimonial-text">Need a solid foundation for your business growth plans? Aria will help you manage sales and meet your current requirements.</p>
                    <p class="testimonial-author">General Manager</p>
                </div>
                <a class="btn-solid-reg" href="#your-link">DETAILS</a> <a class="btn-outline-reg mfp-close as-button" href="#projects">BACK</a> 
            </div> --> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of lightbox-basic -->
    <!-- end of lightbox -->

    <!-- Lightbox -->
    <div id="project-4" class="lightbox-basic zoom-anim-dialog mfp-hide">
        <div class="row">
            <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
            <div class="col-lg-12">
                <img class="img-fluid" src="images/img-4.jpg" alt="alternative">
            </div> <!-- end of col -->
            <!-- <div class="col-lg-4">
                <h3>Van Moose</h3>
                <hr class="line-heading">
                <h6>Strategy Development</h6>
                <p>Need a solid foundation for your business growth plans? Aria will help you manage sales and meet your current needs</p>
                <p>By offering the best professional services and quality products in the market. Don't hesitate and get in touch with us.</p>
                <div class="testimonial-container">
                    <p class="testimonial-text">Need a solid foundation for your business growth plans? Aria will help you manage sales and meet your current requirements.</p>
                    <p class="testimonial-author">General Manager</p>
                </div>
                <a class="btn-solid-reg" href="#your-link">DETAILS</a> <a class="btn-outline-reg mfp-close as-button" href="#projects">BACK</a> 
            </div> --> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of lightbox-basic -->
    <!-- end of lightbox -->

    <!-- Lightbox -->
    <div id="project-5" class="lightbox-basic zoom-anim-dialog mfp-hide">
        <div class="row">
            <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
            <div class="col-lg-12">
                <img class="img-fluid" src="images/img-5.jpg" alt="alternative">
            </div> <!-- end of col -->
           <!--  <div class="col-lg-4">
                <h3>Joy Moments</h3>
                <hr class="line-heading">
                <h6>Strategy Development</h6>
                <p>Need a solid foundation for your business growth plans? Aria will help you manage sales and meet your current needs</p>
                <p>By offering the best professional services and quality products in the market. Don't hesitate and get in touch with us.</p>
                <div class="testimonial-container">
                    <p class="testimonial-text">Need a solid foundation for your business growth plans? Aria will help you manage sales and meet your current requirements.</p>
                    <p class="testimonial-author">General Manager</p>
                </div>
                <a class="btn-solid-reg" href="#your-link">DETAILS</a> <a class="btn-outline-reg mfp-close as-button" href="#projects">BACK</a> 
            </div> --> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of lightbox-basic -->
    <!-- end of lightbox -->

    <!-- Lightbox -->
    <div id="project-6" class="lightbox-basic zoom-anim-dialog mfp-hide">
        <div class="row">
            <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
            <div class="col-lg-12">
                <img class="img-fluid" src="images/img-6.jpg" alt="alternative">
            </div> <!-- end of col -->
            <!-- <div class="col-lg-4">
                <h3>Spark Events</h3>
                <hr class="line-heading">
                <h6>Strategy Development</h6>
                <p>Need a solid foundation for your business growth plans? Aria will help you manage sales and meet your current needs</p>
                <p>By offering the best professional services and quality products in the market. Don't hesitate and get in touch with us.</p>
                <div class="testimonial-container">
                    <p class="testimonial-text">Need a solid foundation for your business growth plans? Aria will help you manage sales and meet your current requirements.</p>
                    <p class="testimonial-author">General Manager</p>
                </div>
                <a class="btn-solid-reg" href="#your-link">DETAILS</a> <a class="btn-outline-reg mfp-close as-button" href="#projects">BACK</a> 
            </div> --> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of lightbox-basic -->
    <!-- end of lightbox -->

    <!-- Lightbox -->
    <div id="project-7" class="lightbox-basic zoom-anim-dialog mfp-hide">
        <div class="row">
            <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
            <div class="col-lg-12">
                <img class="img-fluid" src="images/img-7.jpg" alt="alternative">
            </div> <!-- end of col -->
            <!-- <div class="col-lg-4">
                <h3>Casual Wear</h3>
                <hr class="line-heading">
                <h6>Strategy Development</h6>
                <p>Need a solid foundation for your business growth plans? Aria will help you manage sales and meet your current needs</p>
                <p>By offering the best professional services and quality products in the market. Don't hesitate and get in touch with us.</p>
                <div class="testimonial-container">
                    <p class="testimonial-text">Need a solid foundation for your business growth plans? Aria will help you manage sales and meet your current requirements.</p>
                    <p class="testimonial-author">General Manager</p>
                </div>
                <a class="btn-solid-reg" href="#your-link">DETAILS</a> <a class="btn-outline-reg mfp-close as-button" href="#projects">BACK</a> 
            </div> --> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of lightbox-basic -->
    <!-- end of lightbox -->

    <!-- Lightbox -->
    <div id="project-8" class="lightbox-basic zoom-anim-dialog mfp-hide">
        <div class="row">
            <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
            <div class="col-lg-12">
                <img class="img-fluid" src="images/img-8.jpg" alt="alternative">
            </div> <!-- end of col -->
            <!-- <div class="col-lg-4">
                <h3>Zazoo Apps</h3>
                <hr class="line-heading">
                <h6>Strategy Development</h6>
                <p>Need a solid foundation for your business growth plans? Aria will help you manage sales and meet your current needs</p>
                <p>By offering the best professional services and quality products in the market. Don't hesitate and get in touch with us.</p>
                <div class="testimonial-container">
                    <p class="testimonial-text">Need a solid foundation for your business growth plans? Aria will help you manage sales and meet your current requirements.</p>
                    <p class="testimonial-author">General Manager</p>
                </div>
                <a class="btn-solid-reg" href="#your-link">DETAILS</a> <a class="btn-outline-reg mfp-close as-button" href="#projects">BACK</a> 
            </div> --> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of lightbox-basic -->
    <!-- end of lightbox -->
<!-- Footer -->
<footer id="contact" class="page-footer font-small blue-grey lighten-5">
  <!-- <div class="container text-center text-md-left mt-5">
    <div class="row mt-3 dark-grey-text">
      <div class="col-md-3 col-lg-4 col-xl-3 mb-4">
        <h6 class="text-uppercase font-weight-bold">About Us</h6>
        <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        <p>Here you can use rows and columns to organize your footer content. Lorem ipsum dolor sit amet,
          consectetur
          adipisicing elit.</p>
      </div>
      <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
        <h6 class="text-uppercase font-weight-bold">Contact</h6>
        <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        <p>
          <i class="fas fa-home mr-3"></i> New York, NY 10012, US</p>
        <p>
          <i class="fas fa-envelope mr-3"></i> info@example.com</p>
        <p>
          <i class="fas fa-phone mr-3"></i> + 01 234 567 88</p>
        <p>
          <i class="fas fa-print mr-3"></i> + 01 234 567 89</p>
      </div>
    </div>
  </div> -->
    <div class="footer">
    <div class="container">

      <!-- Grid row-->
      <div style="justify-content: space-between;" class="row py-4 d-flex align-items-center">

        <!-- Grid column -->
        <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
          <span class="mb-0">Copyright © towFixers 2020. All rights reserved.</span>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-6 col-lg-7 text-center text-md-right social_icons">

          <!-- Facebook -->
          <a href="https://www.facebook.com/Towfixers-100396308203697/?modal=admin_todo_tour " style="text-decoration: none" class="fb-ic">
            <i class="fab fa-facebook-f white-text"> </i>
          </a>
          <!-- Twitter -->
          <a href="https://twitter.com/carfixers" style="text-decoration: none" class="tw-ic">
            <i class="fab fa-twitter white-text"> </i>
          </a>
          <!-- Google +-->
          <a style="text-decoration: none" class="gplus-ic">
            <i class="fab fa-google-plus-g white-text"> </i>
          </a>
          <!--Linkedin -->
          <a style="text-decoration: none" class="li-ic">
            <i class="fab fa-linkedin-in white-text"> </i>
          </a>
          <!--Instagram-->
          <a href="https://www.instagram.com/carfixers/" style="text-decoration: none" class="ins-ic">
            <i class="fab fa-instagram white-text"> </i>
          </a>

        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row-->

    </div>
  </div>
</footer>
<div style="z-index: 9998;" class="loader_container" id="loader">
    <div class="loader"></div>  
</div>
<!-- Footer -->
    
    	
    <!-- Scripts -->
    <script src="js/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="js/popper.min.js"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="js/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
    <script src="js/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
    <script src="js/morphext.min.js"></script> <!-- Morphtext rotating text in the header -->
    <script src="js/isotope.pkgd.min.js"></script> <!-- Isotope for filter -->
    <script src="js/validator.min.js"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="js/scripts.js"></script> <!-- Custom scripts -->
</body>
</html>
<script type="text/javascript">
    $('#btnContactus').on('click',function(){
        $('#loader').css("display","flex");
        $('html').css("overflow","hidden");
        var mobileNumber = $('#mobileNumber').val();
        var contact_email = $('#contact_email').val();
        var message = $('#message').val();

        var data = JSON.stringify({
            "mobileNumber":mobileNumber,
            "contact_email": contact_email,
            "message": message,
            });
            request = $.ajax({
                contentType: "application/json; charset=utf-8",
                method: "POST",
                url: "contactus.php",
                data: data,
                dataType : "json",
            });
            request.done(function (response, textStatus, jqXHR){
                if (response.success === 1) {
                    //console.log("Success");
                    alert("Email Successfully Sent.");
                    window.location='index.php';
                }
                else{
                    alert("Something Went Wrong! Please try after sometime!");
                }
            });
            
    });
</script>